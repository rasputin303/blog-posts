# In Defence of Tuples

Tuples are Elm's least-loved data structure. They're sort of like records, but
they don't have nice field names. They're sort of like lists, but they can only
hold a maximum of three elements. What is the use of that?

First, let's deal with the "they can only hold three elements" thing. This is
true of the simple pairs and triples that we tend to think of when we're using
tuples in day-to-day Elm code, like `( 1, "a" )` and `(0.5, True, "b")`. But
there are many ways to create tuples with arbitrary numbers of elements through
_nesting_.

## Nested tuples

```elm
( 1, ( 2, ( 3, 4 ) ) )

( ( 1, 2 ), ( 3, 4 ) )

( ( ( 1, 2 ), 3 ), 4 )
```

The first of these examples is interesting. The first element, `1`, is in the
outermost layer of the tuple, and we get deeper into the tuple as the sequence
continues. Let's rewrite it slightly:

```elm
nestedTuple = 
    ( 1, ( 2, ( 3, ( 4, () ) ) ) )
```

We've wrapped `4` in a tuple with a unit type, `()`, as the second element. With
this structure, we can access the contents of the tuple as follows:

```elm
nestedTuple |> Tuple.first
--> 1

nestedTuple |> Tuple.second |> Tuple.first
--> 2

nestedTuple |> Tuple.second |> Tuple.second |> Tuple.first
--> 3

nestedTuple |> Tuple.second |> Tuple.second |> Tuple.second |> Tuple.first
--> 4
```

To get the first element of the tuple (or to put it another way, the element at
position 0) we use 0 calls of `Tuple.second`, followed by `Tuple.first`. To get
the element at position 1, we use 1 call of `Tuple.second` followed by
`Tuple.first`, and so on. This gives us a hint that we can manipulate this type
of nested tuple structure programmatically/recursively to do all sorts of
interesting things...

## Building nested tuples

Let's see how we could make a nice builder-style API to build up a nested tuple,
without having to type all those nasty parentheses ourselves.

```elm
start = 
    ()

add element builder = 
    ( element, builder )

nestedTuple = 
    start
        |> add 1
        |> add 2.0
        |> add "3"

--> ( "3", ( 2.0, ( 1, () ) ) )
```

Hmm, that's a bit back-to-front. We really wanted the `1` to be the first
element. What if we do this instead:

```elm
start = 
    \next -> next -- or `identity`

add element builder = 
    \next -> builder ( element, next )

end builder = 
    builder ()

nestedTuple = 
    start
        |> add 1
        |> add 2.0
        |> add "3"
        |> end

--> ( 1, ( 2.0, ( "3", () ) ) )
```

It's a bit less elegant, as we now need an `end` function to finish off the
pipeline, and it's not as easy to understand the code, but at least it gives us
what we want. Ship it!

## Operating on nested tuples

Now, imagine we want to write some code that will not only build up a nested
tuple, but also provide a function that will convert all the elements into
strings and then join the strings together into a single string (because... why
_wouldn't_ you want to do that? It's not a contrived example at all.)

Let's define some `toString` functions for our different element types:

```elm
int = 
    String.fromInt

float = 
    String.fromFloat

string = 
    identity
```

Now, let's revisit our tuple builder:

```elm
start = 
    { tuple = identity 
    , toStringFunctions = identity
    , toStringMaker = identity 
    }

add toString element builder = 
    { tuple = 
        -- Just as before, we build up our nested tuple from the elements 
        -- provided.
        \next -> builder.tuple ( element, next )
    , toStringFunctions = 
        -- Now, we also build up a nested tuple that contains the `toString` 
        -- functions for each of the elements.
        \next -> builder.toStringFunctions ( toString, next )
    , toStringMaker = 
        -- Finally, we build up a function that will apply the `toString` 
        -- functions to the tuple elements and collect the resulting strings in 
        -- a list.
        \next -> builder.toStringMaker (toStringMaker next)
    }

toStringMaker next acc (toString, restToStrings) (element, restElements) =
    -- This is a bit of a head-scratcher, but you can think of it a bit like a
    -- `fold` function that:
    --
    -- 1. Takes an accumulator (`acc`) and two nested tuples
    --
    -- 2. Extracts the first element of each tuple
    --
    -- 3. Does something with the extracted elements (in this case, it 
    --    applies the `element` value from the second tuple to the `toString` 
    --    function from the first tuple and then prepends the result to `acc`,
    --    which is a `List String`) 
    --
    -- 4. Calls a `next` function with the new accumulator and the remaining 
    --    elements of each tuple. 
    -- 
    -- If the `next` function is _also_ `toStringMaker`, then it will do the 
    -- same thing on the next elements of the two tuples. So, by composing 
    -- multiple calls of `toStringMaker`, we gain the ability to recurse into a 
    -- nested tuple of arbitrary depth.
    -- 
    -- Once we've got to the end of the elements in our nested tuples, we need
    -- to pass a different type of function as `next`, which will stop recursing
    -- and output our accumulator. You will see an example of this `final` 
    -- function in the `end` function below, as the first argument to 
    -- `builder.toStringMaker`.
    next (toString element :: acc) restToStrings restElements

end builder = 
    { tuple = builder.tuple () 
    , toString = 
        -- We now combine the built-up `toStringFunctions` and `toStringMaker` 
        -- to create a function that takes a nested tuple and turns it into a 
        -- string.
        \tuple -> 
            builder.toStringMaker 
                -- The first argument is the `final` function described above. 
                -- It will receive the accumulator (a list of all the strings 
                -- we've created from our tuple elements), the final 
                -- element of the nested tuple of toString functions (`()`), and 
                -- the final element of our nested tuple of values (also 
                -- `()`). All it needs to do is discard the two `()`s and 
                -- combine the accumulated list into a single string.
                (\acc () () -> acc |> List.reverse |> String.join ", ")
                -- The second argument is the initial (empty) list that we'll 
                -- use as the accumulator to collect the stringified tuple 
                -- elements.
                []
                -- The third argument is the nested tuple of toString functions.
                (builder.toStringFunctions ())
                -- And the final argument is the nested tuple of elements that 
                -- we want to convert into a string.
                tuple
    }

example = 
    start
        |> add int 1
        |> add float 2.0
        |> add string "3"
        |> end

example.tuple 
--> ( 1, ( 2.0, ( "3", () ) ) )

example.toString example.tuple
--> "1, 2, 3"
```

## A simplification

Something that only occurred to me as I was writing this article is that we
don't actually need to build up the `toStringFunctions` nested tuple -
there's a simpler way.

Since we pass the appropriate `toString` function for each element when we call
`add`, we can bake it into the `toStringMaker` function straight away, instead
of collecting all the `toStringFunctions` in a nested tuple and only applying 
them to `toStringMaker` when we call `end`.

The key change is in the `toStringMaker` function:

```elm
-- Old

toStringMaker next acc (toString, restToStrings) (element, restElements) =
    next (toString element :: acc) restToStrings restElements

-- New

toStringMaker toString next acc (element, restElements) =
    next (toString element :: acc) restElements
```

Anything we know about at `add`-time can be placed _before_ the `next` argument,
while anything we only know about at `end`-time or runtime has to go _after_
`next`.

In our new version, we will pass `toString` into `toStringMaker` at `add`-time,
so it can come before `next` and we won't need to pass it in again at
`end`-time. 

By contrast the nested tuple of elements still needs to go after `next`, because
we want the user to be able to pass an arbitrary `( Int, ( Float, ( String, () )
) )` value to our final `toString` function at runtime.

The full code looks like this:

```elm
start = 
    { tuple = identity 
    -- We no longer need the `toStringFunctions` field here.
    , toStringMaker = identity 
    }

add toString element builder = 
    { tuple = 
        \next -> builder.tuple ( element, next )
    , toStringMaker = 
        -- We bake `toString` into `toStringMaker` at `add`-time here:
        \next -> builder.toStringMaker (toStringMaker toString next)
    }

toStringMaker toString next acc (element, restElements) =
    next (toString element :: acc) restElements

end builder = 
    { tuple = builder.tuple () 
    , toString = 
        \tuple -> 
            -- Note how `builder.toStringMaker` has now become a function that 
            -- takes three arguments, not four, because we no longer need to 
            -- pass in the `toStringFunctions` nested tuple. 
            --
            -- It's also very important to notice that our `final` function now 
            -- takes one `()`, not two, because we're now only recursing through 
            -- one nested tuple, not two.
            builder.toStringMaker 
                (\acc () -> acc |> List.reverse |> String.join ", ")
                []
                tuple
    }
```
