# Open and closed builders in Elm

## The builder pattern

The builder pattern is a nice way of building and configuring data structures in Elm. There is an excellent description on Sebastian Porto's [Elm Patterns](https://sporto.github.io/elm-patterns/basic/builder-pattern.html) site, and the pattern has also been discussed in detail in [episode 8](https://elm-radio.com/episode/builder-pattern/) of the wonderful [Elm Radio](https://elm-radio.com/) podcast.

One of my favourite explorations of the advantages of the builder pattern is Brian Hicks' highly entertaining talk, "[Robot Buttons from Mars](https://www.youtube.com/watch?v=PDyWP-0H4Zo)". 

In this talk, Brian explains how the builder pattern (which he calls the `with*` pattern) ticks a lot of boxes in terms of sound API design. He argues that the builder pattern:

* __Produces consistency__ - it provides a single abstraction that makes it easy for the user to build the data structures they need, instead of reinventing the wheel and building them from scratch in multiple different places in the codebase.

* __Enables variation within constraints__ - it makes it easy for the user to create all the different variations of a data structure that their application needs. At the same time, it can bake in some rules to prevent users from creating invalid versions of the data structure.

* __Provides good defaults__ - it makes life easy for the user by only requiring them to specify a minimal set of attributes, and supplying sensible (yet overridable) defaults for the rest.

* __Prevents misuse__ - it "just works" as expected, and users can use it correctly without needing to understand any of its internal implementation details.

Sounds pretty good, right? I've certainly found that the builder pattern has all these advantages in my own projects. 

## What does a builder API look like?

Here's an example:

```elm
myButton =
    button "Click me!" ButtonClicked
    |> withSize Large
    |> withColour Red
    |> toHtml
```

We start with a function (`button`) that defines an initial data structure that we'll use as our builder. If necessary, the user may need to supply a few required attributes as arguments to this function (in this case, the label for the button, "Click me!", and the `Msg` that will be sent to the Elm runtime when the button is clicked).

We can then choose to pipe the result of this initial fuction through a series of `with*` functions in order to set additional (optional) attributes (in this case, the size and colour of the button).

Finally, we pipe into a function that converts (or "builds") the data structure into the final type that we want. In this case, `toHtml` converts the data structure into an `Html Msg` so that it can be rendered in Elm's view function.

## How does it work?

A typical implementation might look something like this:

```elm
type Button msg 
    = Button 
        { label : String
        , onClick : msg
        , colour : Colour 
        , size : Size
        , ... -- other fields such as `border`, `font`, etc.
        }

{- The `button` function creates our builder data structure with its 
   default attributes. Here it specifies that the default colour is
   `Red` and the default size is `Medium`.
   
   A couple of the fields - `label` and `onClick` - don't really have 
   any sensible defaults that we can supply for ourselves. So here we
   ask the user to pass in `label` and `msg` as required arguments.
-}
button : String -> msg -> Button msg
button label msg =
    Button 
        { label = label
        , onClick = msg
        , colour = Red
        , size = Medium
        , ...  -- default values for other fields here
        }

{- All the `with*` functions all look a bit like this - they 
   update/override one of the fields in the `Button msg` data 
   structure and return a new `Button msg`
-}
withColour : Colour -> Button msg -> Button msg
withColour colour (Button b) = 
    Button { b | colour = colour }

{- Finally, we have a "closing" function, which simply converts our 
   button data structure into an `Html msg`
-}
toHtml : Button msg -> Html msg
toHtml (Button b) = 
    -- convert Button to Html somehow
    ...
```

## Reuse and variation

With this setup, you can not only create and configure buttons easily, you can also _reuse_ the configuration of one button when configuring another by calling additional `with*` functions to override some of the first button's attributes:

```elm
type Msg 
    = FirstButtonClicked
    | SecondButtonClicked


myFirstButton : Button Msg
myFirstButton = 
    button "Click me!" FirstButtonClicked 
        |> withColour Green


mySecondButton : Button Msg
mySecondButton = 
    myFirstButton
        |> withLabel "Tap me!"
        |> withOnClick SecondButtonClicked
```

In this case, `myFirstButton` inherits the size (`Medium`) of the default `button`, but overrides the default colour (`Red`) with a new colour (`Green`). `mySecondButton` then inherits the green colour of `myFirstButton`, but overrides its label and onClick behaviour. This ability to inherit and override can be extremely helpful to keep code brief, simple and easy to read when building complex UI components with many customisable attributes.

## A slight snag

So far, this all seems very clean and lovely, but there's a slight snag when you come to use it in practice. Imagine you have configured a bunch of buttons and want to render them in your view:

```elm
view : Html Msg
view = 
    Html.div [] 
        [ myFirstButton 
            |> toHtml
        , mySecondButton 
            |> toHtml
        , anotherButton
            |> toHtml
        , oneLastButton
            |> toHtml
        ]
```

Every time you use a button, you have to pipe it to the `toHtml` function in order to convert it from its "builder" representation (`Button msg`) to a value of the type that `Html.div` is expecting (`Html msg`).

This might seem like a minor niggle, but when you have to do it for every button in a large application, it becomes annoying. 

Moreover, if you really like the builder pattern, it's probably not going to stop at buttons. You might also want a text builder, an icon builder, a chart builder, a table builder, and any number of other builders for different UI elements. You're going to have to call some kind of `toHtml` function on every single one of them, every time you use it.

## Can't we just bake it in?

OK, so what if we bake `toHtml` into the definition of `myFirstButton`?

```elm
{- Notice how the type of `myFirstButton` is now `Html Msg`, not `Button
   Msg`
-}
myFirstButton : Html Msg
myFirstButton = 
    button ButtonClicked 
        |> withLabel "Click me!"
        |> withColour Green
        |> toHtml

view : Html Msg 
view = 
    Html.div [] 
        [ myFirstButton
        , ...
        ]
```

This makes our view code much less fussy. Yay!

## Uh oh!

Unfortunately, while our view is now [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself) as a good martini, the improvement comes at a terrible price. Now that we've called `toHtml`, we can no longer continue to customise `myFirstButton` by piping it into any of our `with*` functions. 

As a result, if we try to use `myFirstButton` as a base configuration for our other buttons, it won't work. If we try the following, we'll now get a compiler error:

```elm
mySecondButton : Button Msg
mySecondButton = 
    myFirstButton -- this is now of type `Html Msg`...
        |> withLabel "Tap me!" -- ...but this is expecting a `Button Msg`
        |> withOnClick SecondButtonClicked
```

Alas, `myFirstButton` is no longer our nice configurable `Button msg` type. We've irretrievably converted it into `Html msg`, an opaque type whose internals we can no longer tinker with.

## Open and closed builders

I'm not sure if there's already a term for what I want to talk about here, so I'm going to invent some terminology. 

Let's agree that any pipeline of functions that results in a builder type (such as our `Button msg` type) is an example of an "open" builder. 

By contrast, any builder pipeline that ends with a function that converts a builder type to another type (such as our `toHtml` function) is an example of a "closed" builder.

As we've seen, in an ideal world, we'd always want to be dealing with open builders - we want to keep things configurable forever. 

## You can't always get what you want

Unfortunately, in practice, if we're using builders to create UI elements, we can't keep them open forever. 

The only type that the Elm runtime can render in its `view` function is `Html msg`. So we have no choice but to close our builder with `toHtml` if we want to render it in the view. Sad times!

However, there is a possible compromise. What if we _delay_ the conversion from open to closed until the last possible moment?

## Delaying the inevitable

How about trying something like this:

```elm
{- Instead of `Button msg`, our builder is now an `Element msg` type
   with different variants for each of the types of element in our UI.
-}
type Element msg 
    = Button 
        { label : String
        , onClick : msg
        , colour : Colour
        , size : Size 
        }
    | Div 
        (List (Html.Attribute msg)) 
        (List (Element msg))


{- `button` now returns a variant of `Element msg` -}
button : String -> msg -> Element msg
button label msg =
    Button 
        { label = label
        , onClick = msg
        , colour = Red
        , size = Medium 
        }


{- Instead of using `Html.div`, we now define our own `div` function, 
   which also returns a variant of `Element msg` 
-}
div : List (Html.Attribute msg) -> List (Element msg) -> Element msg
div attrs children =
    Div attrs children


{- All our `with*` functions now need to check whether we're dealing
   with a `Button` or another type of element. 
-}
withColour : Colour -> Element msg -> Element msg
withColour colour el = 
    case el of
        Button b -> 
            Button { b | colour = colour }
        
        _ -> 
            el


{- `toHtml` can now convert both `Button`s and `Div`s to `Html msg` -}
toHtml : Element msg -> Html msg
toHtml element = 
    case element of
        Button options msg label -> 
            -- convert button to Html somehow
            ...

        Div attrs children -> 
            -- convert div to Html and recursively convert 
            -- its children to Html too
            Html.div attrs (List.map toHtml children)
```

Notice that the type of `button` has changed - it's no longer `Button msg` but a variant of `Element msg`. And our new `div` function also produces an `Element msg`. 

Since all of our UI elements (`button`s and `div`s) are now of the same type, we no longer have to convert each child element (`button`) to the type of its parent (`div`). 

So instead of calling `toHtml` on each button individually, we can simply call `toHtml` once on the root `div` of our entire view.

```elm
view : Html Msg
view = 
    div [] 
        [ myFirstButton 
        , mySecondButton
        , anotherButton
        , oneLastButton
        ]
        |> toHtml
```

Much nicer!

## Summing up

Opaque types such as `Html msg` or `Cmd msg` are fundamental building blocks of the Elm architecture, and opaque types in general are a powerful tool for library and application authors to build APIs that limit the risk of bugs and help to make impossible states impossible.

However, from a user's perspective, opaque types can sometimes be a bit of a nuisance. Once you've created an `Html msg`, you're limited in what you can do with it, and there's no way to convert it back into a more flexible type.

The builder pattern provides an elegant way to create and configure data structures. The final step of a builder's pipeline often involves calling a function that converts the builder type into one of these opaque types - or in our terminology, converts an open builder into a closed builder.

In this article, we've attempted to demonstrate the advantages of deferring this "closing" step for as long as possible. By keeping our builders open for as long as possible, we can increase configurability and reusability, while keeping our code terse and readable.

## An exercise for the reader

Of course, we can go much further than this. Our implementation of `div`, for example, still takes a list of `Html.Attributes`, which isn't very builder-y. With a bit of jiggery-pokery, we could probably end up with an API more like this:

```elm
view : Html Msg
view = 
    div
        |> withLayout Row
        |> withSpacing 10
        |> withChildren 
            [ myFirstButton 
            , mySecondButton
            , div
                |> withLayout Column
                |> withPadding 20
                |> withChildren 
                    [ anotherButton
                    , oneLastButton
                    ]
            ]
        ]
```

Or who knows, maybe something even nicer! Let me know what you come up with. An entire builder pattern-based alternative API for [elm-ui](https://package.elm-lang.org/packages/mdgriffith/elm-ui/latest/), perhaps? Consider yourselves nerd-sniped 😈.